# Wasm Webpack Boilerplate 
## Allows you to check out how wasm allows developers to interoperate between rust and the web

### Need Rust installed
#### Run Rustup and make sure you have the wasm target installed on your computer
```
rustup target "wasm32-unknown-unknown"
```
### Node.js installed
#### Install all the dependencies required
```
npm install
```

#### Build Rust library
```
cargo build --target wasm32-unknown-unknown
```

### Test it with the dev command
```
npm run dev
```
The build should start and open a new window at localhost:9000